export interface ProtectedRouteProps {
    exact: boolean;
    path: string;
    component: any;
}
