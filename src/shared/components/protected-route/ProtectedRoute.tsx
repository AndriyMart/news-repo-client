import { Route, Redirect } from "react-router-dom";
import React, { useContext } from "react";

import { ProtectedRouteProps } from "./models/ProtectedRouteProps";
import { UserStatus } from "../../services/auth/models/UserStatus";
import { AuthContext } from "../../services/auth/AuthProvider";
import PageLoader from "../page-loader/PageLoader";

export default function ProtectedRoute({component: Component, ...rest}: ProtectedRouteProps) {
    const {currentUser} = useContext(AuthContext);

    const render = (props: any) => {
        switch (currentUser) {
            case UserStatus.NotDefined:
                return (<PageLoader />);
            case UserStatus.Unathorized:
                return (<Redirect to={"/signin"} />);
            default:
                return (<Component {...props} />);
        }
    };

    return (
      <Route
          {...rest}
          render={render}
      />
    );
}

