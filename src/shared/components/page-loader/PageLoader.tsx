import { Spinner } from "react-bootstrap";
import { nanoid } from "nanoid";
import React from "react";

import styles from "./PageLoader.module.scss";

export default function PageLoader() {

    const spinners = Array(3)
        .fill(null)
        .map(() => <Spinner
            className={styles.Spinner}
            animation="grow"
            variant="primary"
            size="sm"
            key={nanoid()}
        />);

    return (<div className={styles.MainWrap}>
        {spinners}
    </div>);
}
