import firebase from "firebase";

export class AuthService {

    public static SignIn(email: string, password: string)
    : Promise<firebase.auth.UserCredential> {
        const auth = firebase.auth();

        return auth.signInWithEmailAndPassword(email, password);
    }

    public static SignOut()
    : Promise<void> {
        const auth = firebase.auth();

        return auth.signOut();
    }
}
