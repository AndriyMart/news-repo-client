import React, { useEffect, useState } from "react";
import firebase from "firebase";
import "firebase/auth";

import { ProviderProps } from "./models/ProviderProps";
import { AuthProviderValue } from "./models/AuthProviderValue";
import { UserStatus } from "./models/UserStatus";

export const AuthContext = React.createContext<AuthProviderValue>({currentUser: UserStatus.NotDefined});

export const AuthProvider = ({children}: ProviderProps ) => {
    const [currentUser, setCurrentUser] = useState<firebase.User | UserStatus>(UserStatus.NotDefined);

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            setCurrentUser(user || UserStatus.Unathorized);
        });
    }, []);

    return(
      <AuthContext.Provider
          value={{currentUser}}
          >
        {children}
        </AuthContext.Provider>
    );
}

