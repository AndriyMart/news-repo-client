import { UserStatus } from "./UserStatus";
import firebase from "firebase";
import User = firebase.User;

export interface AuthProviderValue {
    currentUser: User | UserStatus;
}
