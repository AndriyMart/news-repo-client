import { registerLocale, setDefaultLocale } from "react-datepicker";
import uk from "date-fns/locale/uk";

export class LocaleService {

    public static SetDefaultLocale()
    : void {
        registerLocale("uk", uk)
        setDefaultLocale("uk");
    }
}
