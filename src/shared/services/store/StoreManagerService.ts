import firebase from "firebase";

import { WhereQueryConfig } from "./models/WhereQueryConfig";

export class StoreManagerService {

    private db: firebase.firestore.Firestore;

    constructor() {
        this.db = firebase.firestore();
    }

    public async getAll<T>(collectionName: string)
        : Promise<T[]> {
        const res: T[] = [];
        const querySnapshot = await this.db.collection(collectionName).get();

        querySnapshot.forEach(i => {
            const instance = i.data();
            instance.id = i.id;

            res.push(instance as T);
        });

        return res;
    }

    public async getWhere<T, ConfigValueType>(collectionName: string, configs: WhereQueryConfig<ConfigValueType>[])
    : Promise<T[]> {
        const res: T[] = [];

        let collection: firebase.firestore.Query = this.db.collection(collectionName);

        configs.forEach(config => {
            const { fieldPath, opStr, value } = config;
            collection = collection.where(fieldPath, opStr, value);
        });

        const querySnapshot = await collection
            .get();

        querySnapshot.forEach(i => res.push(i.data() as T));
        return res;
    }
}
