import { StoreManagerService } from "../StoreManagerService";
import { NewsFilterModel } from "../../../../components/news-filter/models/NewsFilterModel";
import { NewsRecord } from "../../../models/NewsRecord";
import { WhereQueryConfig } from "../models/WhereQueryConfig";

export class NewsRecordsStoreService {

    public static readonly CollectionName = "news";

    private storeManager : StoreManagerService;

    constructor() {
        this.storeManager = new StoreManagerService();
    }

    public getFilteredNewsRecords(filter: NewsFilterModel)
    : Promise<NewsRecord[]> {
        const dateRange = NewsRecordsStoreService.ComposeOneDateRange(filter.date);

        const configs: WhereQueryConfig<string>[] =  [{
                fieldPath: "snapshotTime",
                opStr: ">=",
                value: dateRange[0].toISOString(),
            },
            {
                fieldPath: "snapshotTime",
                opStr: "<=",
                value: dateRange[1].toISOString(),
            },
            {
                fieldPath: "resourceId",
                opStr: "==",
                value: filter.resource,
            }];

        return this.storeManager.getWhere<NewsRecord, string>(NewsRecordsStoreService.CollectionName, configs)
    }

    public async getAllNewsRecordsForDate(date: Date)
    : Promise<NewsRecord[]> {
        const dateRange = NewsRecordsStoreService.ComposeOneDateRange(date);

        const configs: WhereQueryConfig<string>[] =  [{
                fieldPath: "snapshotTime",
                opStr: ">=",
                value: dateRange[0].toISOString(),
            },
            {
                fieldPath: "snapshotTime",
                opStr: "<=",
                value: dateRange[1].toISOString(),
            }];

        return this.storeManager.getWhere<NewsRecord, string>(NewsRecordsStoreService.CollectionName, configs)
    }

    private static ComposeOneDateRange(date: Date)
    : Date[] {
    const dayStart = new Date(date);
        dayStart.setHours(0,0,0,0);

        const dayEnd = new Date(date);
        dayEnd.setHours(23,59,59,999);

        return [dayStart, dayEnd];
    }
}
