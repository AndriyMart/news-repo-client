import { StoreManagerService } from "../StoreManagerService";
import { Resource } from "../../../models/Resource";

export class ResourcesStoreService {

    public static readonly CollectionName = "resources";

    private storeManager: StoreManagerService;

    constructor() {
        this.storeManager = new StoreManagerService();
    }

    public async getResources()
    : Promise<Resource[]> {
        return this.storeManager.getAll<Resource>(ResourcesStoreService.CollectionName);
    }
}
