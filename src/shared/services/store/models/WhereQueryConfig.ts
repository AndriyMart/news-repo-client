import firebase from "firebase";

export interface WhereQueryConfig<T> {

    fieldPath: string | firebase.firestore.FieldPath,
    opStr: "<" | "<=" | "==" | "!=" | ">=" | ">" | "array-contains" | "in" | "not-in" | "array-contains-any",
    value: T;
}
