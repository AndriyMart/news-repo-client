import firebase from "firebase/app";
import "firebase/auth";

export class FirebaseService {

    private static readonly firebaseConfig = {
        apiKey: "AIzaSyCLyzdsdzUVA_rda-rbqi8Dqc27P3-QtPw",
        authDomain: "news-repository.firebaseapp.com",
        projectId: "news-repository",
    };

    public static InitIfNeeded()
    : void {
        if (firebase.apps.length === 0) {
            firebase.initializeApp(this.firebaseConfig);
        }
    }
}
