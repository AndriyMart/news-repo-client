export interface NewsRecord {
    heading: string;
    resourceId: string;
    snapshotTime: string;
    url: string;
}
