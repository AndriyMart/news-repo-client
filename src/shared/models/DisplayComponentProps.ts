export interface DisplayComponentProps<T> {
    value: T;
}
