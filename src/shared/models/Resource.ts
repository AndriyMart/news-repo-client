export interface Resource {
    id: string;
    name: string;
    url?: string;
    feedUrl?: string;
    logoPath?: string;
}
