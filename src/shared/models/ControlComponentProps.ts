export interface ControlComponentProps<T = any> {
    value?: T;
    onChange: Function;
}
