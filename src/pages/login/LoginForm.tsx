import { ToastContainer, toast } from "react-toastify";
import React, { FormEvent, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";

import { AuthService } from "../../shared/services/auth/AuthService";
import styles from "./LoginForm.module.scss";

export default function LoginForm() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();

    const validateForm = () => email.length > 0 && password.length > 0;

    const handleSubmit = (event: FormEvent) => {
        event.preventDefault();

        AuthService.SignIn(email, password)
           .then(
               () => setTimeout(() => history.push("/")),
               err => toast.error(err.message),
           );
    }

    return (
        <div className={styles.Login}>

            <Form
                onSubmit={handleSubmit}
            >

                <Form.Group
                    controlId="email"
                >
                    <Form.Label>
                        Е-пошта
                    </Form.Label>
                    <Form.Control
                        size="sm"
                        autoFocus
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group
                    controlId="password"
                >
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control
                        size="sm"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>

                <Button block size="sm" type="submit" disabled={!validateForm()}>
                    Увійти
                </Button>
            </Form>

            <ToastContainer />
        </div>
    );
}
