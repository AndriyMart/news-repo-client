import React, { useEffect, useState } from "react";

import NewsPagination, { defaultPageIndex, itemsPerPage } from "../../components/news-pagination/NewsPagination";
import { NewsRecordsStoreService } from "../../shared/services/store/news-records/NewsRecordsStoreService";
import NewsFilter, { defaultNewsFilter } from "../../components/news-filter/NewsFilter";
import { defaultResourceOption } from "../../components/news-filter/components/ResourceSelect";
import { NewsFilterModel } from "../../components/news-filter/models/NewsFilterModel";
import LayoutBlock from "../../components/layout/block/LayoutBlock";
import NewsList from "../../components/news-list/NewsList";
import { NewsRecord } from "../../shared/models/NewsRecord";
import styles from "./NewsPage.module.scss";

export default function NewsPage() {
    const [ filter, filterChange ] = useState(defaultNewsFilter);
    const [ pagination, paginationChange ] = useState(defaultPageIndex);
    const [ records, recordsChange ] = useState<NewsRecord[]>([]);
    const [ filteredRecords, filteredRecordsChange ] = useState<NewsRecord[]>([]);

    const onFilterChange = (value: NewsFilterModel) => filterChange(value);
    const onPaginationChange = (value: number) => paginationChange(value);

    useEffect( () => { updateRecords(); }, [filter]);
    useEffect(() => { updateFilteredRecords(); }, [pagination, records]);

    return (
        <LayoutBlock>
            <div className={styles.MainWrap}>
                <NewsFilter
                    value={filter}
                    onChange={onFilterChange}
                />

                <NewsList value={filteredRecords} />
                <NewsPagination
                    value={pagination}
                    totalCount={records.length}
                    onChange={onPaginationChange}
                />
            </div>
        </LayoutBlock>
    );

    // region Local Functions

    function resetPagination()
    : void {
        paginationChange(defaultPageIndex);
    }

    function updateFilteredRecords()
    : void {
        const fromIndex = (pagination - 1) * itemsPerPage;
        const toIndex = fromIndex  + itemsPerPage;

        const paginatedRecords = records.slice(fromIndex, toIndex);
        const sortedRecords = paginatedRecords.sort((a, b) =>
            +new Date(a.snapshotTime) - +new Date(b.snapshotTime));

        filteredRecordsChange(sortedRecords);
    }

    async function updateRecords()
    : Promise<void> {
        const storeService = new NewsRecordsStoreService();

        const loadedRecords = filter.resource === defaultResourceOption.id
            ? await storeService.getAllNewsRecordsForDate(filter.date)
            : await storeService.getFilteredNewsRecords(filter);

        recordsChange(loadedRecords);
        resetPagination();
    }

    // endregion
}
