import { NewsRecord } from "../../../shared/models/NewsRecord";

export interface NewsItemProps {
    value: NewsRecord;
    resourceLogoUrl: string;
}
