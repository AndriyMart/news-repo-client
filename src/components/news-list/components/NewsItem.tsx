import React from "react";

import { NewsItemProps } from "../models/NewsItemProps";
import styles from "../NewsList.module.scss";

export default function NewsItem(props: NewsItemProps) {

    return <a
        href={props.value.url}
        className={styles.NewsItem}
        target="_blank"
        rel="noreferrer"
    >
        <span>
            {props.value.heading}
        </span>

        <img
            className={styles.ResourceLogo}
            src={props.resourceLogoUrl}
            alt=""
        />
    </a>;
}
