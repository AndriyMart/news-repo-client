import React, { useEffect, useState } from "react";
import firebase from "firebase";
import { nanoid } from "nanoid";

import { ResourcesStoreService } from "../../shared/services/store/resources/ResourcesStoreService";
import { DisplayComponentProps } from "../../shared/models/DisplayComponentProps";
import { Dictionary } from "../../shared/models/Dictionary";
import { NewsRecord } from "../../shared/models/NewsRecord";
import { Resource } from "../../shared/models/Resource";
import NewsItem from "./components/NewsItem";
import styles from "./NewsList.module.scss";

export default function NewsList(props: DisplayComponentProps<NewsRecord[]>) {
    const [resources, resourcesChange] = useState<Resource[]>([]);
    const [resourceLogoUrls, resourceLogoUrlsChange] = useState<Dictionary<string>>({});

    useEffect(() => {
        loadResources();
    }, []);
    useEffect(() => {
        updateResourceUrls();
    }, [resources])

    const records = props.value.map((record) =>
        <NewsItem
            value={record}
            key={nanoid()}
            resourceLogoUrl={resourceLogoUrls[record.resourceId]}
        />);

    return <div className={styles.MainWrap}>
        {records}
    </div>;

    // region Local Functions

    async function updateResourceUrls()
    : Promise<void> {
        const dictionary: Dictionary<string> = {};
        const storage = firebase.storage();

        await Promise.all(resources.map(async (resource) =>
            dictionary[resource.id] = await storage
                .refFromURL(resource.logoPath as string).getDownloadURL()));

        resourceLogoUrlsChange(dictionary);
    }

    async function loadResources()
    : Promise<void> {
        const storeService = new ResourcesStoreService();

        const resourcesData = await storeService.getResources();
        resourcesChange(resourcesData);
    }

    // endregion
}
