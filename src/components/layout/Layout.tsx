import { Switch, Route, BrowserRouter } from "react-router-dom";
import React from "react";
import "firebase/auth";

import ProtectedRoute from "../../shared/components/protected-route/ProtectedRoute";
import { AuthProvider } from "../../shared/services/auth/AuthProvider";
import LoginForm from "../../pages/login/LoginForm";
import NewsPage from "../../pages/news/NewsPage";
import styles from "./Layout.module.scss";
import Header from "./header/Header";

export function Layout() {
    return (
        <BrowserRouter basename="/">
            <div>
                <Header/>

                <div className={styles.PageWrap}>

                    <AuthProvider>
                        <Switch>
                            <ProtectedRoute
                                exact
                                path="/"
                                component={NewsPage}
                            />
                            <Route
                                exact
                                path="/signin"
                                component={LoginForm}
                            />
                        </Switch>
                    </AuthProvider>
                </div>
            </div>
        </BrowserRouter>
    );
}
