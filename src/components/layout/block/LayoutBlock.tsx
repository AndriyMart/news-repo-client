import { ProviderProps } from "../../../shared/services/auth/models/ProviderProps";
import styles from "./LayoutBlock.module.scss";
import React from "react";

export default function LayoutBlock(props: ProviderProps) {
    const { children } = props;

    return <div className={styles.MainWrap}>
        {children}
    </div>;
}
