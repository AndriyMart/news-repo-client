import React from "react";

import HeaderControls from "../../header-controls/HeaderControls";
import { Link } from "react-router-dom";
import logo from "../../../assets/logo.svg";
import styles from "./Header.module.scss";

export default function Header() {

    return (
        <div className={styles.HeaderWrap}>

            <div className={styles.Header}>

                <Link to="/">
                    <img
                        className={styles.Logo}
                        src={logo}
                        alt="News Repo"
                    >
                    </img>
                </Link>

                <HeaderControls />
            </div>
        </div>
    );
}
