import React from "react";

import { FirebaseService } from "../shared/services/firebase/FirebaseService";
import { LocaleService } from "../shared/services/locale/LocaleService";
import { Layout } from "./layout/Layout";
import "../assets/styles/Styles.scss";

function App() {
  FirebaseService.InitIfNeeded();
  LocaleService.SetDefaultLocale();

  return (
    <Layout/>
  );
}

export default App;
