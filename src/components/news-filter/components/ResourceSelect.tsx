import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";

import { ResourcesStoreService } from "../../../shared/services/store/resources/ResourcesStoreService";
import { ControlComponentProps } from "../../../shared/models/ControlComponentProps";
import { Resource } from "../../../shared/models/Resource";
import styles from "../NewsFilter.module.scss";

export default function ResourceSelect(props: ControlComponentProps<string>) {
    const [resourceOptions, setResourceOptions] = useState<Resource[]>([defaultResourceOption]);
    const { value, onChange } = props;

    useEffect( () => {
        loadResourceOptions();
    }, [onChange]);


    const resourceSelectOptions = buildSelectOptions();
    const onOptionSelect = (event: any) => onChange(event.target.value);

    return <Form.Group
        controlId="resource"
        className={styles.FormGroup}
    >
        <Form.Label className="mr-2 mb-0">
            Ресурс
        </Form.Label>

        <Form.Control
            size="sm"
            as="select"
            value={value}
            onChange={onOptionSelect}
        >
            {resourceSelectOptions}
        </Form.Control>
    </Form.Group>

    // region Local Functions

    function buildSelectOptions()
    : JSX.Element[] {
        return resourceOptions.map((option) =>
            <option
                key={option.id}
                value={option.id}
            >
                {option.name}
            </option>);
    }

    async function loadResourceOptions()
    : Promise<void> {
        const storeService = new ResourcesStoreService();
        const options = await storeService.getResources()

        setResourceOptions([defaultResourceOption, ...options]);
    }

    // endregion
}

export const defaultResourceOption = {
    id: "default",
    name: "Всі"
};
