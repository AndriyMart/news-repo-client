import DatePicker from "react-datepicker";
import { Form } from "react-bootstrap";
import React  from "react";

import { ControlComponentProps } from "../../../shared/models/ControlComponentProps";
import styles from "../NewsFilter.module.scss";

export default function DateSelect(props: ControlComponentProps) {
    const { onChange, value } = props;

    const onDateChange = (date: Date) => {
        onChange(date);
    };

    const maxDate = new Date();

    return <Form.Group
        controlId="date"
        className={styles.FormGroup}
    >
        <Form.Label className="mr-2 mb-0">
            Дата
        </Form.Label>
        <DatePicker
            selected={value}
            onChange={onDateChange}
            popperPlacement="bottom-start"
            calendarClassName={styles.CalendarWrap}
            dateFormat={"dd MMMM yyyy"}
            className={"form-control form-control-sm"}
            maxDate={maxDate}
        />
    </Form.Group>;
}
