export interface NewsFilterModel {
    date: Date;
    resource: string;
}
