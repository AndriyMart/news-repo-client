import React from "react";

import ResourceSelect, { defaultResourceOption } from "./components/ResourceSelect";
import { ControlComponentProps } from "../../shared/models/ControlComponentProps";
import { NewsFilterModel } from "./models/NewsFilterModel";
import DateSelect from "./components/DateSelect";
import styles from "./NewsFilter.module.scss";

export default function NewsFilter(props: ControlComponentProps<NewsFilterModel>) {
    const { onChange, value } = props;

    const onDateSelect = (value: Date) => onChange(updateModel({date: value}));
    const onResourceSelect = (value: string) => onChange(updateModel({resource: value}));

    return <div className={styles.MainWrap}>

        <DateSelect
            value={value?.date}
            onChange={onDateSelect}
        />

        <ResourceSelect
            value={value?.resource}
            onChange={onResourceSelect}
        />
    </div>;

    // region local Functions

    function updateModel(partial: Partial<NewsFilterModel>)
    : NewsFilterModel {
        return {
            ...props.value,
            ...partial,
        } as NewsFilterModel;
    }

    // endregion
}

export const defaultNewsFilter: NewsFilterModel = {
    date: new Date(),
    resource: defaultResourceOption.id
};
