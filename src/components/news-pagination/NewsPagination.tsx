import React, { useEffect, useState } from "react";
import { Pagination } from "react-bootstrap";
import { nanoid } from "nanoid";

import styles from "./NewsPagination.module.scss";
import { NewsPaginationProps } from "./models/NewsPaginationProps";

export default function NewsPagination(props: NewsPaginationProps) {
    const { value, totalCount, onChange } = props;
    const [ pagesCount, pagesCountChange ] = useState(0);

    useEffect(
        () => { pagesCountChange(Math.ceil(totalCount / itemsPerPage)) },
        [totalCount]
    );

    const onPaginationSelect = (event: any) => { onChange(+event.target.attributes.value.value) };

    const items = getItems();

    return <div className={styles.MainWrap}>
        <Pagination size="sm">
            {items}
        </Pagination>
    </div>;

    // region Local Functions

    function getItems()
    : JSX.Element[] {
        return Array(pagesCount)
            .fill(null)
            .map((e, index) => {
                const pageNum = index + 1;

                return <Pagination.Item
                    key={nanoid()}
                    active={pageNum === value}
                    onClick={onPaginationSelect}
                    value={pageNum}
                >
                    {pageNum}
                </Pagination.Item>
            });
    }

    // endregion
}

export const defaultPageIndex = 1;
export const itemsPerPage = 20;
