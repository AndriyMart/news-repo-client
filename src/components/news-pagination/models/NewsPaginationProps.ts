export interface NewsPaginationProps {
    value: number;
    totalCount: number;
    onChange: Function;
}
