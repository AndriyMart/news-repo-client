import React, { useEffect, useState } from "react";
import firebase from "firebase";
import { FaSignOutAlt } from "react-icons/all";

import { UserStatus } from "../../shared/services/auth/models/UserStatus";
import { AuthService } from "../../shared/services/auth/AuthService";
import styles from "./HeaderControls.module.scss";

export default function HeaderControls() {
    const [currentUser, setCurrentUser] = useState<firebase.User | UserStatus>(UserStatus.NotDefined);

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            setCurrentUser(user || UserStatus.Unathorized);
        });
    }, []);

    const onSignOut = () => AuthService.SignOut();

    if (currentUser !== UserStatus.Unathorized && currentUser !== UserStatus.NotDefined) {
        return <div className={styles.MainWrap}>
            <div
                className={styles.IconWrap}
                title="Вийти з профілю"
                onClick={onSignOut}
            >
                <FaSignOutAlt/>
            </div>
        </div>;
    }

    return <div></div>;
}
